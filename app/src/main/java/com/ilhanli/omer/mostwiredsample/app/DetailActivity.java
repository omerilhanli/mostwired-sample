package com.ilhanli.omer.mostwiredsample.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ilhanli.omer.mostwiredsample.BaseActivity;
import com.ilhanli.omer.mostwiredsample.R;
import com.ilhanli.omer.mostwiredsample.model.Article;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import butterknife.BindView;

public class DetailActivity extends BaseActivity {

    private static final String LINK = "link";

    @BindView(R.id.btn_geri)
    Button btnGeri;
    @BindView(R.id.btn_tekrarlayanlar)
    Button btnTekrarlayanlar;
    @BindView(R.id.tv_baslik)
    TextView tvBaslik;
    @BindView(R.id.tv_icerik)
    TextView tvIcerik;


    private String link;

    @Override
    public int getLayoutId() {
        return R.layout.activity_detail;
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    public static void getSpiritActivity(Context context, String link) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(LINK, link);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent() != null) {

            link = getIntent().getExtras().getString(LINK);

            prepareDialog("Article detail is loading..");

            AsenkronArticleLoaders async = new AsenkronArticleLoaders();

            async.execute(link);
        }
    }

    public class AsenkronArticleLoaders extends AsyncTask<String, String, Article> {

        @Override
        protected Article doInBackground(String... params) {

            try {
                Article article = new Article();
                String link = params[0];

                Connection connection = Jsoup.connect(link);
                connection.timeout(15000);
                Document doc = connection.get();

                Elements elements = doc.getAllElements();
                for (int i = 0; i < elements.size(); i++) {
                    Element el = elements.get(i);
                    if (el.tagName().equals("h1")) {
                        article.setTitle(el.ownText().trim()); // BAŞLIK
                        System.out.println(el.ownText() + "\n");
                        break;
                    }
                }

                ArrayList<Element> pList = new ArrayList<>();
                for (int i = 0; i < elements.size(); i++) {
                    Element el = elements.get(i);
                    Element elP = el.select("p").first(); // İçerik
                    Element elH3 = el.select("h3").first(); // Ara başlıklar

                    if (elP != null && !pList.contains(elP)) {
                        if (!elP.ownText().equals("CNMN Collection")) {
                            pList.add(elP);
                        } else {
                            break;
                        }
                    }

                    if (elH3 != null && !pList.contains(elH3)) {
                        pList.add(elH3);
                    }
                }

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < pList.size(); i++) { // MAKALE
                    Element element = pList.get(i);
                    if (!element.ownText().trim().equals(" ")) {
                        builder.append(element.text().trim());
                    }
                }
                article.setContent(builder.toString());
                return article;

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(final Article article) {
            super.onPostExecute(article);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            });

            if (article == null) return;

            tvBaslik.setText(article.getTitle());
            tvIcerik.setText(article.getContent());

            btnTekrarlayanlar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MostRepeatWordActivity.getSpiritActivity(DetailActivity.this, article.getContent());
                }
            });
        }
    }

}
