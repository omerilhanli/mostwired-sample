package com.ilhanli.omer.mostwiredsample.model;

/**
 * Created by omerilhanli on 17.06.2017.
 */

public class Repeat {

    private String word;
    private int total;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }


}
