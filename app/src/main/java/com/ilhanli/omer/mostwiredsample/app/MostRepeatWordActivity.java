package com.ilhanli.omer.mostwiredsample.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

import com.ilhanli.omer.mostwiredsample.BaseActivity;
import com.ilhanli.omer.mostwiredsample.R;
import com.ilhanli.omer.mostwiredsample.model.Repeat;
import com.ilhanli.omer.mostwiredsample.utils.GoogleTranslate;
import com.ilhanli.omer.mostwiredsample.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MostRepeatWordActivity extends BaseActivity {

    private final static String CONTENT="content";

    private String content;
    private List<Repeat> mostRepeatList;

    @BindView(R.id.tv_tekrar_edenler)
    TextView tvTekrarEdenler;

    public static void getSpiritActivity(Context context, String content) {
        Intent intent = new Intent(context, MostRepeatWordActivity.class);
        intent.putExtra(CONTENT, content);
        context.startActivity(intent);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_most_repeat_word;
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        content = getIntent().getExtras().getString(CONTENT);

        mostRepeatList = findList(content);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Translate Api is process..");
        progressDialog.show();

        new AsenkronArticleLoaders().execute("");
    }

    public List<Repeat> findList(String content) {
        List<Repeat> repeatList = Utils.getFindMostWords(content);
        Repeat oldTkrMax = null;
        List<Repeat> tmpTkList5 = new ArrayList<>();
        return Utils.findMax(oldTkrMax, tmpTkList5, repeatList);
    }

    public class AsenkronArticleLoaders extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {

            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < mostRepeatList.size(); i++) {
                Repeat mostRepeat = mostRepeatList.get(i);
                GoogleTranslate translator = new GoogleTranslate(getString(R.string.google_translate_api_key));
                String text = "";
                if (mostRepeat.getWord().equals("the")) {
                    text = "the";
                } else {
                    text = translator.translate(mostRepeat.getWord(), "en", "tr");
                }

                builder.append(mostRepeat.getTotal() + " defa \'" + mostRepeat.getWord() + "\' -->> anlamı : \'" + text + "\' \n\n");
            }

            return builder.toString();
        }

        @Override
        protected void onPostExecute(final String tekrarlayanlar) {
            super.onPostExecute(tekrarlayanlar);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            });

            if (tekrarlayanlar.equals("")) return;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tvTekrarEdenler.setText(tekrarlayanlar);
                }
            });
        }
    }

}
