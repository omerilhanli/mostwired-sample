package com.ilhanli.omer.mostwiredsample;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import butterknife.ButterKnife;

/**
 * Created by omerilhanli on 17.06.2017.
 */

public abstract class BaseActivity extends AppCompatActivity {

    public abstract int getLayoutId();

    public abstract Activity getActivity();

    public ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getLayoutId());

        ButterKnife.bind(getActivity());

        checkInternet();
    }

    public void finish(View view) {
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopDialog();
    }

    public void stopDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public void prepareDialog(String mssgTitle){
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(mssgTitle);
        progressDialog.show();
    }

    public void infoAlert(String msg) {
        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);

        alertDialogBuilder.setTitle("Uyarı");
        alertDialogBuilder.setIcon(ContextCompat.getDrawable(this, android.R.drawable.ic_dialog_alert));
        alertDialogBuilder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Anladım", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        getActivity().finish();
                    }
                });
        // create alert dialog
        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void checkInternet() {
        if (!isNetworkConnected()) {
            infoAlert("Connection was lost!");
            return;
        }
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

}
