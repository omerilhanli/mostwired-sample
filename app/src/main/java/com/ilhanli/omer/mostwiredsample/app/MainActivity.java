package com.ilhanli.omer.mostwiredsample.app;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;


import com.ilhanli.omer.mostwiredsample.BaseActivity;
import com.ilhanli.omer.mostwiredsample.R;
import com.ilhanli.omer.mostwiredsample.adapter.ArticleRecyclerAdapter;
import com.ilhanli.omer.mostwiredsample.model.ArticleShort;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MainActivity extends BaseActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prepareDialog("Most Recently Articles are loading...");

        AsenkronArticleLoaders async = new AsenkronArticleLoaders();

        async.execute();
    }

    public void prepareRecyclerView(List<ArticleShort> articleShorts) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getBaseContext());
        layoutManager.setOrientation(LinearLayout.VERTICAL);
        layoutManager.scrollToPosition(0);
        recyclerView.setLayoutManager(layoutManager);

        ArticleRecyclerAdapter mAdapter = new ArticleRecyclerAdapter(MainActivity.this, articleShorts);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    public class AsenkronArticleLoaders extends AsyncTask<String, String, ArrayList<ArticleShort>> {

        @Override
        protected ArrayList<ArticleShort> doInBackground(String... params) {
            try {
                ArrayList<ArticleShort> articleShortList = new ArrayList<>();

                String adr = "https://www.wired.com/most-recent/";
                Connection connection = Jsoup.connect(adr);
                connection.timeout(10000);
                Document doc = connection.get();

                Elements list = doc.select(getString(R.string.most_recently_links_tag));
                for (int i = 0; i < list.size(); i++) {
                    ArticleShort articleShort = new ArticleShort();

                    Element el = list.get(i);
                    String link = el.select("a").first().attr("abs:href");
                    articleShort.setLink(link);
                    String baslik = el.select("a > h2").first().ownText();
                    articleShort.setTitle(baslik);
                    String ozet = el.select("a > p").first().ownText();
                    articleShort.setSummary(ozet);

                    articleShortList.add(articleShort);
                }
                return articleShortList;

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(ArrayList<ArticleShort> articleShorts) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            });

            if (articleShorts == null) return;

            prepareRecyclerView(articleShorts);
        }
    }

}
