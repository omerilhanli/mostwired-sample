package com.ilhanli.omer.mostwiredsample.utils;

import com.ilhanli.omer.mostwiredsample.model.Repeat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by omerilhanli on 17.06.2017.
 */

public class Utils {
    public static List<Repeat> findMax(Repeat oldTkrMax, List<Repeat> tkList5, List<Repeat> repeatList) {

        Repeat tkrMax = findMin(repeatList);

        if (tkList5.isEmpty() && oldTkrMax != null) {
            tkrMax.setTotal(oldTkrMax.getTotal());
            tkrMax.setWord(oldTkrMax.getWord());
        } else if (oldTkrMax == null) {
            for (int i = 0; i < repeatList.size(); i++) {
                Repeat tkr = repeatList.get(i);
                if (tkrMax.getTotal() < tkr.getTotal()
                        && !check5(tkList5, tkr)) {
                    tkrMax.setTotal(tkr.getTotal());
                    tkrMax.setWord(tkr.getWord());
                }
            }
        } else {
            for (int i = 0; i < repeatList.size(); i++) {
                Repeat tkr = repeatList.get(i);
                boolean hasIntoList = check5(tkList5, tkr);
                if (!hasIntoList
                        && !tkrMax.getWord().equals(tkr.getWord())
                        && tkrMax.getTotal() <= tkr.getTotal()) {
                    tkrMax.setTotal(tkr.getTotal());
                    tkrMax.setWord(tkr.getWord());
                }
            }
        }

        tkList5.add(tkrMax);
        if (tkList5.size() < 5) {
            oldTkrMax = new Repeat();
            oldTkrMax.setTotal(tkrMax.getTotal());
            oldTkrMax.setWord(tkrMax.getWord());
            findMax(oldTkrMax, tkList5, repeatList);
        } else {
            System.out.println("tkList5 : " + tkList5.size());
            return tkList5;
        }
        return tkList5;
    }

    public static boolean check5(List<Repeat> tkr5List, Repeat tkr) {
        if (tkr5List.isEmpty()) {
            return false;
        }
        for (int j = 0; j < tkr5List.size(); j++) {
            Repeat tkr5 = tkr5List.get(j);
            if (tkr.getWord().equals(tkr5.getWord()) && tkr.getTotal() == tkr5.getTotal()) {
                return true;
            }
        }

        return false;
    }

    public static Repeat findMin(List<Repeat> repeatList) {
        Repeat tkMin = new Repeat();
        tkMin.setWord(repeatList.get(0).getWord());
        tkMin.setTotal(repeatList.get(0).getTotal());
        for (int i = 0; i < repeatList.size(); i++) {
            Repeat tkr = repeatList.get(i);
            if (tkMin.getTotal() > tkr.getTotal()) {
                tkMin.setWord(tkr.getWord());
                tkMin.setTotal(tkr.getTotal());
            }
        }

        return tkMin;
    }

    public static ArrayList<Repeat> getFindMostWords(String text) {
        ArrayList<Repeat> repeatList = new ArrayList();

        String[] wordArr = text.split(" ");
        for (int i = 0; i < wordArr.length; i++) {
            String eleman = wordArr[i];
            Repeat repeat = new Repeat();

            int adet = 0;
            for (int j = 0; j < wordArr.length; j++) {
                String subEleman = wordArr[j];
                if (!eleman.equals(" ") && eleman.equals(subEleman)) {
                    adet++;
                }
            }
            repeat.setWord(eleman);
            repeat.setTotal(adet);
            boolean has = false;
            if (repeatList.isEmpty()) {
                repeatList.add(repeat);
            } else {
                for (int j = 0; j < repeatList.size(); j++) {
                    Repeat tk = repeatList.get(j);
                    String tkKelime = tk.getWord();
                    int tkAdet = tk.getTotal();
                    String tekrarKelime = repeat.getWord();
                    int tekrarAdet = repeat.getTotal();
                    if (tekrarKelime.equals(tkKelime) && tekrarAdet == tkAdet) {
                        has = true;
                        break;
                    }
                }

                if (!has) {
                    repeatList.add(repeat);
                }
            }
        }

        for (int i = 0; i < repeatList.size(); i++) {
            Repeat tk = repeatList.get(i);
        }

        int[] tekrarArr = new int[repeatList.size()];
        for (int i = 0; i < tekrarArr.length; i++) {
            tekrarArr[i] = repeatList.get(i).getTotal();
        }

        ArrayList<Repeat> tkList5 = new ArrayList<>();
        Arrays.sort(tekrarArr);
        for (int i = tekrarArr.length - 1; i >= tekrarArr.length - 5; i--) {
            int num = tekrarArr[i];

            for (int j = 0; j < repeatList.size(); j++) {
                Repeat tk = repeatList.get(j);
                if (tk.getTotal()== num) {
                    tkList5.add(tk);
                    break;
                }
            }
        }

        System.out.println("----------");
        for (int i = 0; i < tekrarArr.length; i++) {
            System.out.println(tekrarArr[i]);
        }

        System.out.println("-------------");
        for (int i = 0; i < repeatList.size(); i++) {
            System.out.println(repeatList.get(i).getWord() + " : " + repeatList.get(i).getTotal());
        }

        return repeatList;
    }

}
