package com.ilhanli.omer.mostwiredsample.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ilhanli.omer.mostwiredsample.R;
import com.ilhanli.omer.mostwiredsample.app.DetailActivity;
import com.ilhanli.omer.mostwiredsample.model.ArticleShort;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by omerilhanli on 17.06.2017.
 */

public class ArticleRecyclerAdapter extends RecyclerView.Adapter<ArticleRecyclerAdapter.ViewHolder> {

    private List<ArticleShort> articleShortList;
    private Context context;


    public ArticleRecyclerAdapter(Context context, List<ArticleShort> articleShortList) {
        this.articleShortList = articleShortList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ArticleShort aShort = articleShortList.get(position);
        TextView baslik = holder.tvBaslik;
        TextView ozet = holder.tvOzet;

        baslik.setText(aShort.getTitle());
        ozet.setText(aShort.getSummary());

        holder.rlItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DetailActivity.getSpiritActivity(context, aShort.getLink());
            }
        });
    }

    @Override
    public int getItemCount() {
        if (articleShortList.size() > 5)
            return 5;
        else
            return articleShortList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_baslik)
        TextView tvBaslik;
        @BindView(R.id.tv_ozet)
        TextView tvOzet;
        @BindView(R.id.relative)
        RelativeLayout relative;
        @BindView(R.id.view1)
        View view1;
        @BindView(R.id.view2)
        View view2;
        @BindView(R.id.rl_item)
        RelativeLayout rlItem;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
