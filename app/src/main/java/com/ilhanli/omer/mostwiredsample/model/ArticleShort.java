package com.ilhanli.omer.mostwiredsample.model;

/**
 * Created by omerilhanli on 17.06.2017.
 */

public class ArticleShort {

    private String link;
    private String title;
    private String summary;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
