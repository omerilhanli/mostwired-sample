package com.ilhanli.omer.mostwiredsample.model;

/**
 * Created by omerilhanli on 17.06.2017.
 */

public class Article {

    private String title;
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
