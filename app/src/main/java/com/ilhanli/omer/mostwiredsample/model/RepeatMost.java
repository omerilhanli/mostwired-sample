package com.ilhanli.omer.mostwiredsample.model;

/**
 * Created by omerilhanli on 17.06.2017.
 */

public class RepeatMost {

    private String word;
    private int number;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
